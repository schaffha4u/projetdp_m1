package exodecorateur_angryballs.maladroit.modele;

import java.awt.Graphics;
import java.util.Vector;

import mesmaths.geometrie.base.Vecteur;

public interface Bille {
	/**
	 * @return the position
	 */
	public abstract Vecteur getPosition();
	/**
	 * @return the rayon
	 */
	public abstract double getRayon();
	/**
	 * @return the vitesse
	 */
	public abstract Vecteur getVitesse();
	/**
	 * @return the accélération
	 */
	public abstract Vecteur getAccélération();
	/**
	 * @return the clef
	 */
	public abstract int getClef();
	public abstract double masse();
	
	public abstract void  déplacer(double deltaT);
	public abstract void gestionAccélération(Vector<Bille> billes);
	
	public abstract boolean gestionCollisionBilleBille(Vector<Bille> billes);
	public abstract void collisionContour(double abscisseCoinHautGauche, double ordonnéeCoinHautGauche, double largeur, double hauteur);
	public abstract void dessine (Graphics g);
	public abstract String toString();
}
