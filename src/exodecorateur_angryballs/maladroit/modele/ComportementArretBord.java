package exodecorateur_angryballs.maladroit.modele;

import mesmaths.cinematique.Collisions;

public class ComportementArretBord extends DecorateurBilleNonCumulable {

	public ComportementArretBord(Bille billeDecoree) {
		super(billeDecoree);
	}

	@Override
	public void collisionContour(double abscisseCoinHautGauche, double ordonnéeCoinHautGauche, double largeur,
			double hauteur) {
		Collisions.collisionBilleContourAvecArretHorizontal(this.getPosition(), this.getRayon(), this.getVitesse(), abscisseCoinHautGauche, largeur);
		Collisions.collisionBilleContourAvecArretVertical(this.getPosition(), this.getRayon(), this.getVitesse(), ordonnéeCoinHautGauche, hauteur);
	}

}
