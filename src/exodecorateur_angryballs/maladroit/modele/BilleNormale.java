package exodecorateur_angryballs.maladroit.modele;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Vector;

import mesmaths.cinematique.Cinematique;
import mesmaths.cinematique.Collisions;
import mesmaths.geometrie.base.Geop;
import mesmaths.geometrie.base.Vecteur;

public class BilleNormale implements Bille {
	public  Vecteur position;   // centre de la bille
	public  double rayon;            // rayon > 0
	public  Vecteur vitesse;
	public  Vecteur accélération;
	public int clef;                // identifiant unique de cette bille

	private Color couleur;

	private static int prochaineClef = 0;

	public static double ro = 1;        // masse volumique
	
	/**
	 * @param centre
	 * @param rayon
	 * @param vitesse
	 * @param accélération
	 * @param couleur
	 */
	public BilleNormale(Vecteur centre, double rayon, Vecteur vitesse,
	        Vecteur accélération, Color couleur)
	{
	this.position = centre;
	this.rayon = rayon;
	this.vitesse = vitesse;
	this.accélération = accélération;
	this.couleur = couleur;
	this.clef = BilleNormale.prochaineClef++;
	}

	/**
	 * @param position
	 * @param rayon
	 * @param vitesse
	 * @param couleur
	 */
	public BilleNormale(Vecteur position, double rayon, Vecteur vitesse, Color couleur)
	{
		this(position,rayon,vitesse,new Vecteur(),couleur);
	}
	
	/**
	 * @return the position
	 */
	public Vecteur getPosition()
	{
	return this.position;
	}
	/**
	 * @return the rayon
	 */
	public double getRayon()
	{
	return this.rayon;
	}
	/**
	 * @return the vitesse
	 */
	public Vecteur getVitesse()
	{
	return this.vitesse;
	}
	/**
	 * @return the accélération
	 */
	public Vecteur getAccélération()
	{
	return this.accélération;
	}
	/**
	 * @return the clef
	 */
	public int getClef()
	{
	return this.clef;
	}
	public double masse() {return ro*Geop.volumeSphère(rayon);}
	
	public void dessine (Graphics g) {
		int width, height;
	    int xMin, yMin;
	    
	    xMin = (int)Math.round(position.x-rayon);
	    yMin = (int)Math.round(position.y-rayon);

	    width = height = 2*(int)Math.round(rayon); 

	    g.setColor(couleur);
	    g.fillOval( xMin, yMin, width, height);
	    g.setColor(Color.CYAN);
	    g.drawOval(xMin, yMin, width, height);
	}
	
	public String toString() 
    {
		return "centre = " + position + " rayon = "+rayon +  " vitesse = " + vitesse + " accélération = " + accélération + " couleur = " + couleur + "clef = " + clef;
    }

	@Override
	public void déplacer(double deltaT) {
		Cinematique.mouvementUniformémentAccéléré( this.getPosition(), this.getVitesse(), this.getAccélération(), deltaT);
	}

	@Override
	public void gestionAccélération(Vector<Bille> billes) {
		this.getAccélération().set(Vecteur.VECTEURNUL);
	}

	@Override
	public boolean gestionCollisionBilleBille(Vector<Bille> billes) {
		return OutilsBille.gestionCollisionBilleBille(this, billes);
	}

	@Override
	public void collisionContour(double abscisseCoinHautGauche, double ordonnéeCoinHautGauche, double largeur,
			double hauteur) {
		Collisions.collisionBilleContourAvecRebond( this.getPosition(), this.getRayon(), this.getVitesse(), abscisseCoinHautGauche, ordonnéeCoinHautGauche, largeur, hauteur);		
	}
}
