package exodecorateur_angryballs.maladroit.modele;

import java.util.Vector;

public class ComportementMvtNewton extends DecorateurBilleCumulable {

	public ComportementMvtNewton(Bille billeDecoree) {
		super(billeDecoree);
	}
	
	@Override
	public void gestionAccélération(Vector<Bille> billes) {
		super.billeDecoree.gestionAccélération(billes);
		this.getAccélération().ajoute(OutilsBille.gestionAccélérationNewton(this, billes));
	}
}
