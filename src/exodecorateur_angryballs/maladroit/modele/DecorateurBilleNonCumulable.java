package exodecorateur_angryballs.maladroit.modele;

public abstract class DecorateurBilleNonCumulable extends DecorateurBille {
	/*
	 * @throw IllegalArgumentException : Exception lev�e si on d�cide de cumuler des comportements non cumulables.
	 */ 
	public DecorateurBilleNonCumulable(Bille billeDecoree) {
		super(billeDecoree);
		
		if (alreadyNonCumulable(billeDecoree))
			throw new IllegalArgumentException("Un d�corateur non cumulable a d�j� �t� ajout� � la bille !");
	}
	
	private boolean alreadyNonCumulable(Bille billeDecoree) {
		if (billeDecoree instanceof DecorateurBille) {
			DecorateurBille decorateurCourant = (DecorateurBille) billeDecoree;
			while(decorateurCourant instanceof DecorateurBille) {
				if (decorateurCourant instanceof DecorateurBilleNonCumulable)
					return true;
				if (!(decorateurCourant.billeDecoree instanceof DecorateurBille))
					return false;
				decorateurCourant = (DecorateurBille) decorateurCourant.billeDecoree;
			}
		}
		return false;
	}
}