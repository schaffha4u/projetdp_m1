package exodecorateur_angryballs.maladroit.modele;

import java.awt.event.MouseEvent;

public abstract class ControleurEtat
{
	ControleurGeneral controleurGeneral;
	public ControleurEtat suivant,retour; // lien vers l'�tat suivant ou vers l'�tat pr�c�dent
	
	public ControleurEtat(ControleurGeneral controleurGeneral,
	        ControleurEtat suivant, ControleurEtat retour)
	{
		super();
		this.controleurGeneral = controleurGeneral;
		this.suivant = suivant;
		this.retour = retour;
	}
	
	public void mousePressed(MouseEvent e) {}
	
	public void mouseReleased(MouseEvent e) {}
	
	public void mouseDragged(MouseEvent e) {}
}