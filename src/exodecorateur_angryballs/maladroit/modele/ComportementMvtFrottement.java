package exodecorateur_angryballs.maladroit.modele;

import java.util.Vector;

import mesmaths.mecanique.MecaniquePoint;

public class ComportementMvtFrottement extends DecorateurBilleCumulable{

	public ComportementMvtFrottement(Bille billeDecoree) {
		super(billeDecoree);
	}

	@Override
	public void gestionAcc�l�ration(Vector<Bille> billes) {
		super.billeDecoree.gestionAcc�l�ration(billes);                              // remise � z�ro du vecteur acc�l�ration
		this.getAcc�l�ration().ajoute(MecaniquePoint.freinageFrottement(this.masse(), this.getVitesse()));      // contribution de l'acc�l�ration due au frottement dans l'air
	}
}
