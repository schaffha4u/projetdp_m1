package exodecorateur_angryballs.maladroit.modele;

import java.awt.Graphics;
import java.util.Vector;

import mesmaths.geometrie.base.Vecteur;

public abstract class DecorateurBille implements Bille {
	protected Bille billeDecoree;
	
	public DecorateurBille(Bille billeDecoree) {
		this.billeDecoree = billeDecoree;
	}
	
	/**
	 * @return the position
	 */
	public Vecteur getPosition()
	{
		return billeDecoree.getPosition();
	}
	/**
	 * @return the rayon
	 */
	public double getRayon()
	{
	return billeDecoree.getRayon();
	}

	/**
	 * @return the vitesse
	 */
	public Vecteur getVitesse()
	{
	return billeDecoree.getVitesse();
	}

	/**
	 * @return the accélération
	 */
	public Vecteur getAccélération()
	{
	return billeDecoree.getAccélération();
	}
	/**
	 * @return the clef
	 */
	public int getClef()
	{
	return billeDecoree.getClef();
	}
	public double masse() {
		return billeDecoree.masse();
	}
	
	public void dessine (Graphics g) {
		this.billeDecoree.dessine(g);
	}
	
	public String toString() 
    {
		return this.billeDecoree.toString();    
    }
	
	@Override
	public void déplacer(double deltaT) {
		this.billeDecoree.déplacer(deltaT);
	}

	@Override
	public void gestionAccélération(Vector<Bille> billes) {
		this.billeDecoree.gestionAccélération(billes);
	}

	@Override
	public boolean gestionCollisionBilleBille(Vector<Bille> billes) {
		return this.billeDecoree.gestionCollisionBilleBille(billes);
	}

	@Override
	public void collisionContour(double abscisseCoinHautGauche, double ordonnéeCoinHautGauche, double largeur,
			double hauteur) {
		this.billeDecoree.collisionContour(abscisseCoinHautGauche, ordonnéeCoinHautGauche, largeur, hauteur);
	}
}
