package exodecorateur_angryballs.maladroit.modele;

import java.awt.event.MouseEvent;
import mesmaths.geometrie.base.Vecteur;

public class ControleurGrab extends ControleurEtat{
	public ControleurGrab(ControleurGeneral controleurGeneral, ControleurEtat suivant, ControleurEtat retour)
	{
		super(controleurGeneral, suivant, retour);
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		this.controleurGeneral.ControleurCourant = this.controleurGeneral.ControleurCourant.suivant;
		this.controleurGeneral.billeAttrapee = null;
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		Vecteur newAcc = calculateNewAcc(this.controleurGeneral.billeAttrapee, e);
		this.controleurGeneral.billeAttrapee.getAccélération().ajoute(newAcc);
	}
	
	public Vecteur calculateNewAcc(Bille bille, MouseEvent e) {
		Vecteur mousePosition = new Vecteur(e.getX(), e.getY());
		
		double delta_x = (mousePosition.x - bille.getPosition().x)/(bille.masse());
		double delta_y = (mousePosition.y - bille.getPosition().y)/(bille.masse());
		
		return new Vecteur(delta_x, delta_y);
	}
}
