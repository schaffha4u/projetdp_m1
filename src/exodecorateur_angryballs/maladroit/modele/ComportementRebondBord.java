package exodecorateur_angryballs.maladroit.modele;

import mesmaths.cinematique.Collisions;

public class ComportementRebondBord extends DecorateurBilleNonCumulable {

	public ComportementRebondBord(Bille billeDecoree) {
		super(billeDecoree);
	}
	
	@Override
	public void collisionContour(double abscisseCoinHautGauche, double ordonnéeCoinHautGauche, double largeur,
			double hauteur) {
		Collisions.collisionBilleContourAvecRebond( this.getPosition(), this.getRayon(), this.getVitesse(), abscisseCoinHautGauche, ordonnéeCoinHautGauche, largeur, hauteur);		
	}

}
