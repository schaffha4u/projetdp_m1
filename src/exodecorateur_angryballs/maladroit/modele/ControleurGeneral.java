package exodecorateur_angryballs.maladroit.modele;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Vector;

public class ControleurGeneral implements MouseListener, MouseMotionListener{
	
	public ControleurGrab ControleurGrab;
	public ControleurFree ControleurFree;
	
	ControleurEtat ControleurCourant;
	Vector<Bille> billes;
	Bille billeAttrapee;
	
	public ControleurGeneral(Vector<Bille> billes) {
		this.installeControleurs();
		this.billes = billes;
	}
	
	private void installeControleurs()
	{
	//---------- instanciation des 3 objets contr�leurs ---------------
		this.ControleurGrab = new ControleurGrab(this,null,null);
		this.ControleurFree = new ControleurFree(this,this.ControleurGrab,null);
	
		//--------------------- on �tablit les liens oubli�s entre les contr�leurs ---------------- 
		
		this.ControleurGrab.suivant = this.ControleurFree;
		this.ControleurGrab.retour = this.ControleurFree;
		
		this.ControleurFree.retour = this.ControleurGrab;
	
		//-------------------- on place le contr�leur courant sur l'�tat de d�part dans le graphe ----------
	
		this.ControleurCourant = this.ControleurFree;
	}

	public void setControleurCourant(ControleurEtat controleurCourant)
	{
		this.ControleurCourant = controleurCourant;
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		this.ControleurCourant.mousePressed(e);
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		this.ControleurCourant.mouseReleased(e);
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		this.ControleurCourant.mouseDragged(e);
	}

	@Override
	public void mouseClicked(MouseEvent e) {}

	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}

	@Override
	public void mouseMoved(MouseEvent e) {}
}
