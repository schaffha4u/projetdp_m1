package exodecorateur_angryballs.maladroit.modele;

public abstract class DecorateurBilleCumulable extends DecorateurBille {
	public DecorateurBilleCumulable(Bille billeDecoree) {
		super(billeDecoree);
	}
}
