package exodecorateur_angryballs.maladroit.modele;

import mesmaths.cinematique.Collisions;

public class ComportementPasseMurailles extends DecorateurBilleNonCumulable {

	public ComportementPasseMurailles(Bille billeDecoree) {
		super(billeDecoree);
	}

	@Override
	public void collisionContour(double abscisseCoinHautGauche, double ordonnéeCoinHautGauche, double largeur,
			double hauteur) {
		Collisions.collisionBilleContourPasseMuraille( this.getPosition(), abscisseCoinHautGauche, ordonnéeCoinHautGauche, largeur, hauteur);
	}

}
