package exodecorateur_angryballs.maladroit.modele;

import java.awt.event.MouseEvent;
import java.util.Objects;

import mesmaths.geometrie.base.Geop;
import mesmaths.geometrie.base.Vecteur;

public class ControleurFree extends ControleurEtat {
	
	public ControleurFree(ControleurGeneral controleurGeneral, ControleurEtat suivant, ControleurEtat retour)
	{
		super(controleurGeneral, suivant, retour);
	}
	
	public Bille isBillePressed(MouseEvent e) {
		Vecteur v = new Vecteur(e.getX(), e.getY());
		
		for(Bille bille : this.controleurGeneral.billes) {
			if (Geop.appartientDisque(v, bille.getPosition(), bille.getRayon()))
				return bille;
		}
		return null;
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		Bille bille = isBillePressed(e);
		if (!Objects.isNull(bille)) {
			this.controleurGeneral.ControleurCourant = this.controleurGeneral.ControleurCourant.suivant;
			this.controleurGeneral.billeAttrapee = bille;
		}
	}
}
