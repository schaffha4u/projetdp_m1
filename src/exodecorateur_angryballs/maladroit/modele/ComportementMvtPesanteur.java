package exodecorateur_angryballs.maladroit.modele;

import java.util.Vector;

import mesmaths.geometrie.base.Vecteur;

public class ComportementMvtPesanteur extends DecorateurBilleCumulable{
	Vecteur pesanteur;
	
	public ComportementMvtPesanteur(Bille billeDecoree, Vecteur pesanteur) {
		super(billeDecoree);
		this.pesanteur = pesanteur;
	}

	@Override
	public void gestionAcc�l�ration(Vector<Bille> billes) {
		super.billeDecoree.gestionAcc�l�ration(billes);          // remise � z�ro du vecteur acc�l�ration
		this.getAcc�l�ration().ajoute(this.pesanteur);          // contribution du champ de pesanteur (par exemple)
	}
}
